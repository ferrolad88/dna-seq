#!/usr/bin/perl

# use warnings; #display warnings

if (@ARGV < 3) {

print "
Usage:\n
    perl /md2/01-script/illuminizer_v8/qsub-run-local.pl output_folder subj_ID dataset_folder node(optional)

        # output_folder : 	full path to your output folder without \"/\" character at the end
        # subj_ID  : 		number of jobs (qsub) submitting
        # dataset_folder  : 	number of permutation cycles in each job
        # (optional) node : 		node ID (optional)

        e.g. perl /md2/01-script/illuminizer_v8_bsi/qsub-run-local-partial.pl    /md2/Synaptome_Year2/part2-analysis   SID28750      /md2/Synaptome_Year2/part2     node15


";

exit(0);

}


my $mscr_dir="/md2/01-script/illuminizer_v8_bsi";

my $outdir=$ARGV[0];
my $subjID=$ARGV[1];
my $scr1="$outdir/$subjID/$subjID-run_local.sh";

my $subjIDdigit=$ARGV[1];
$subjIDdigit =~ s/\D//g;
my $fastfilesdir=$ARGV[2];
my $myq="root.q";

if (defined $ARGV[3]) {
	$myq="root.q\@$ARGV[3]";
	}


`qsub -q $myq -o $outdir/$subjID/qsub_run.log -N $subjID.phase1 -t $subjIDdigit-$subjIDdigit -S /bin/sh $scr1 $subjID $outdir/$subjID $fastfilesdir`;
print "Submitting ngs analysis phase1 for:", $subjID, "\n";

