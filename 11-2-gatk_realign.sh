#!/bin/sh
#$ -S /bin/sh
#$ -cwd
#$ -l h_vmem=5G,time=8::

GLOBAL="${run_script}/global_config.sh"

if [[ -e $GLOBAL ]]
then
        . $GLOBAL
else
echo "Global config file not found. Exiting."
        exit 1
fi

#  -U ALLOW_UNINDEXED_BAM \
# -B:indels,VCF ${resdir}/${genome}/indels/1kg.pilot_release.merged.indels.sites.${genome}.${index_file}.vcf 
# -knownsOnly  Don't run 'Smith-Waterman' to generate alternate consenses; use only known indels provided as RODs for constructing the alternate references.

echo "============== 11-2 GATK realignment  ================"
echo " "

date
mkdir -p ${tmp_folder}_realign
# rm -f ${DBSNP_idx}

# Realigning

# Next, we actually run the realigner over the intervals to create a 'cleaned' version of the bam file.

java -Xmx${heap}m -Djava.io.tmpdir\=${tmp_folder}_realign \
  -jar $gatk \
  -I $PWDS/${subjectID}.mq.fxmt.bam \
  -R $REF \
  -T IndelRealigner \
  -targetIntervals $PWDS/${subjectID}.forRealign.intervals \
  --out $PWDS/${subjectID}.realigned.bam \
  -known $DBSNP  \
  -LOD 0.4  \
  -compress 5 \
  -l INFO  \
    -L $ExonFile

#   --default_platform Illumina   \
#   --default_read_group MP1  \

#   -nt $CPUs \
#  -D $DBSNP

# rm -f ${DBSNP_idx}

echo "Realignment over the intervals completed!"
date

