#!/bin/bash
#$ -cwd

export lane_num1="1"
export lane_num2="2"
export lane_num3="3"

# analysis output folder
export sample="${PWDS}/${subjectID}"
export tmp_folder="$sample/tmp"

################ Apps ##############

# script and tools folder
export script_dir="/md2/01-script"
export tools_dir="/md2/02-applications"

export resources="${script_dir}/R"
export PATH=$PATH:$resources


# bwa setting (ref Lexicographically sorted )
# export bwa="${tools_dir}/bwa-0.5.9rc1/bwa"
export bwa="${tools_dir}/bwa-0.5.9/bwa"
export hg19_ref="${tools_dir}/bwa-0.5.9/database/hg19/all_chrs/ucsc.hg19.fasta"

export CPUs=3

# human genome ref
export hg19_ref_kayrotypic_sorted=$hg19_ref
export fai="${tools_dir}/bwa-0.5.9/database/hg19/all_chrs/ucsc.hg19.fasta.fai"

# samtools
export samtools="${tools_dir}/samtools-0.1.6_x86_64-linux/samtools"
export samtoolspath="${tools_dir}/samtools-0.1.6_x86_64-linux"
export classpath=$classpath:$samtoolspath

# bamtools
export bamtools="${tools_dir}/bamtools-fffb428/bin/bamtools"

#bedtools
export intersectBed="${tools_dir}/bedtools/bin/intersectBed"

# GATK
export gatk_old="${tools_dir}/GenomeAnalysisTK-1.0.3471/GenomeAnalysisTK.jar"
export gatk="${script_dir}/GenomeAnalysisTK-1.3-14-g348f2db/GenomeAnalysisTK.jar"
export gatk_analyzecovar="${script_dir}/GenomeAnalysisTK-1.3-14-g348f2db/AnalyzeCovariates.jar"


export REF=$hg19_ref_kayrotypic_sorted
export DBSNP="${script_dir}/files/dbSNP135/dbsnp_135.vcf"
export DBSNP_idx="${script_dir}/files/dbSNP135/dbsnp_135.vcf.idx"


export REFSEQ="${script_dir}/files/refgene/refgene-hg19/refGene-converted.rod"
export REFSEQ_idx="${script_dir}/files/refgene/refgene-hg19/refGene-converted.rod.idx"

export ExonFile="${script_dir}/files/BSI-Synaptome/exon.target.interval_list"
export BaitFile="${script_dir}/files/BSI-Synaptome/bait.interval_list"


# PICARD
export picard="${tools_dir}/picard-tools-1.56"
export classpath=$classpath:$picard
export ExonFilePicard="${script_dir}/files/BSI-Synaptome/exon.target.interval_list.picard"
export BaitFilePicard="${script_dir}/files/BSI-Synaptome/bait.interval_list.picard"


### Java config
export HEAP=8000 # default 4G heap
export heap=$HEAP

### snpEff
export snpEff="${tools_dir}/snpeff/snpEff_2_0_5"




