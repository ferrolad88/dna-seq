#!/bin/sh
#$ -S /bin/sh
#$ -cwd
#$ -l h_vmem=5G,time=8::

GLOBAL="${run_script}/global_config.sh"

if [[ -e $GLOBAL ]]
then
        . $GLOBAL
else
echo "Global config file not found. Exiting."
        exit 1
fi



echo "============== 12-1 GATK Analysis of covariates   ================"
echo " "

echo "Analysis of covariates starting at:"
date

mkdir -p ${tmp_folder}_covar
# rm -f ${DBSNP_idx}

#  -U ALLOW_UNINDEXED_BAM \
# http://www.broadinstitute.org/gsa/gatkdocs/release/org_broadinstitute_sting_gatk_walkers_recalibration_CountCovariatesWalker.html

echo -e "Determine (count) the covariates affecting base quality scores in the initial BAM file, emitting a CSV file when done.  \n"
java -Xmx${heap}m -Djava.io.tmpdir\=${tmp_folder}_covar \
 -jar $gatk \
 -R $REF \
 -l INFO \
 -I $PWDS/${subjectID}.realigned.srt.bam \
 -knownSites $DBSNP \
 -T CountCovariates \
 -nt $CPUs \
 -cov ReadGroupCovariate \
 -cov QualityScoreCovariate \
 -cov CycleCovariate \
 -cov DinucCovariate \
 -recalFile $PWDS/${subjectID}.flt.recal_v1.csv  \
    -L $ExonFile

#   --default_platform Illumina   \
#   --default_read_group MP1  \

# --DBSNP $DBSNP \


# rm -f ${DBSNP_idx}

mkdir -p  $PWDS/analyzeCovar_v1

# from http://www.broadinstitute.org/gsa/wiki/index.php/Base_quality_score_recalibration

java -Xmx${heap}m -Djava.io.tmpdir\=${tmp_folder}_covar \
 -jar ${gatk_analyzecovar} \
 -recalFile $PWDS/${subjectID}.flt.recal_v1.csv  \
 -outputDir $PWDS/analyzeCovar_v1  \
 -ignoreQ 3

#  -resources $resources/ 

rm -rf ${tmp_folder}_covar

echo "Analysis of covariates completed! at:"
date




