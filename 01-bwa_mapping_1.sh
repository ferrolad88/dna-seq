#!/bin/bash
#$ -cwd

# mkdir $PWDSSS

date
echo "============== 01-bwa-mapping ================"
echo " "
echo "align first batch"
$bwa aln -l 30 -t $CPUs \
 $hg19_ref \
 $fastq_path1/$fastq_for_gz \
 > $PWDS/fastq1.$lane_num1.sai \
 2> $PWDS/fastq1.$lane_num1.log

date
echo "align second batch"
$bwa aln -l 30 -t $CPUs \
 $hg19_ref \
 $fastq_path1/$fastq_rev_gz \
 > $PWDS/fastq2.$lane_num1.sai \
 2> $PWDS/fastq2.$lane_num1.log

date
echo "pe alignment "
$bwa sampe  \
 $hg19_ref \
 $PWDS/fastq1.$lane_num1.sai \
 $PWDS/fastq2.$lane_num1.sai \
 $fastq_path1/$fastq_for_gz \
 $fastq_path1/$fastq_rev_gz \
 | gzip > $sample.$lane_num1.aligned.sam.gz \
 2> $sample.$lane_num1.aligned.log


#  -a 500 -o 100000 \
#  -i MPIROOZ$lane_num1 \
#  -m SYNAPTOME \
#  -l LIBRARY \
#  -p ILLUMINA \









