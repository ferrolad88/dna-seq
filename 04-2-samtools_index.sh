#!/bin/bash
#$ -cwd

GLOBAL="${run_script}/global_config.sh"

if [[ -e $GLOBAL ]]
then
        . $GLOBAL
else
echo "Global config file not found. Exiting."
        exit 1
fi

echo "============== 04-2 samtools srt & indexing ================"
echo " "
echo "Sorting and indexing started at:"
date


# index bam
${samtools} index \
  ${PWDS}/${subjectID}.bam

# sort bam
$samtools sort -m 4000000000 \
  $PWDS/${subjectID}.bam \
  $PWDS/${subjectID}.srt

# index sorted bam
$samtools index \
  $PWDS/${subjectID}.srt.bam

# stat
${bamtools} stats \
   -insert \
   -in $PWDS/${subjectID}.srt.bam \
   > $PWDS/${subjectID}.srt.stats



echo "Sorting and indexing ${subjectID}.bam .... done! at:"
date



 
