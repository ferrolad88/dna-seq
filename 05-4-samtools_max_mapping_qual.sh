#!/bin/bash
#$ -cwd

GLOBAL="${run_script}/global_config.sh"

if [[ -e $GLOBAL ]]
then
        . $GLOBAL
else
echo "Global config file not found. Exiting."
        exit 1
fi



inputbam=$PWDS/${subjectID}.rmdup
outputbam=$PWDS/${subjectID}.mq


echo "============== 05-4 samtools max mapping filter ================"
echo " "
echo "Mapping quality filtering .... starting at:"
date


$bamtools   filter \
  -mapQuality ">=60"    \
  -in   ${inputbam}.bam   \
  -out  ${outputbam}.srt.bam

$samtools index ${outputbam}.srt.bam


echo "stat bam"

${bamtools} stats \
   -insert \
   -in ${outputbam}.srt.bam \
   > ${outputbam}.srt.stats

rm -rf ${outputbam}.sam 

echo "Mapping quality filtering .... done at:"
date




