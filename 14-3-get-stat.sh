



for i in 1 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30
do

cat /ngs1a/sample_for_JHU/BP$i/analysis/coverage/BP$i.hg18.coverage.dept.sample_summary | head -n 2 | tail -n 1 | cut -f 2 >> bases_on_target.txt
cat /ngs1a/sample_for_JHU/BP$i/analysis/coverage/BP$i.hg18.coverage.dept.sample_summary | head -n 2 | tail -n 1 | cut -f 3 >> mean.txt

cat /ngs1a/sample_for_JHU/BP$i/analysis/coverage/BP$i.hg18.coverage.dept.sample_cumulative_coverage_counts | head -n 2 | tail -n 1 | cut -f 3 >> covered_1x_count.txt
cat /ngs1a/sample_for_JHU/BP$i/analysis/coverage/BP$i.hg18.coverage.dept.sample_cumulative_coverage_proportions | head -n 2 | tail -n 1 | cut -f 3 >> covered_1x_percent.txt

cat /ngs1a/sample_for_JHU/BP$i/analysis/coverage/BP$i.hg18.coverage.dept.sample_cumulative_coverage_counts | head -n 2 | tail -n 1 | cut -f 12 >> covered_10x_count.txt
cat /ngs1a/sample_for_JHU/BP$i/analysis/coverage/BP$i.hg18.coverage.dept.sample_cumulative_coverage_proportions | head -n 2 | tail -n 1 | cut -f 12 >> covered_10x_percent.txt

cat /ngs1a/sample_for_JHU/BP$i/analysis/coverage/BP$i.hg18.coverage.dept.sample_cumulative_coverage_counts | head -n 2 | tail -n 1 | cut -f 42 >> covered_40x_count.txt
cat /ngs1a/sample_for_JHU/BP$i/analysis/coverage/BP$i.hg18.coverage.dept.sample_cumulative_coverage_proportions | head -n 2 | tail -n 1 | cut -f 42 >> covered_40x_percent.txt


done


 


