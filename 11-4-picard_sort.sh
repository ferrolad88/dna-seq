#!/bin/sh
#$ -S /bin/sh
#$ -cwd
#$ -l h_vmem=5G,time=8::

GLOBAL="${run_script}/global_config.sh"

if [[ -e $GLOBAL ]]
then
        . $GLOBAL
else
echo "Global config file not found. Exiting."
        exit 1
fi


mv $PWDS/${subjectID}.realigned.fxmt.bam $PWDS/${subjectID}.realigned.srt.bam
mv $PWDS/${subjectID}.realigned.fxmt.bai $PWDS/${subjectID}.realigned.srt.bai

${bamtools} stats \
   -insert \
   -in $PWDS/${subjectID}.realigned.srt.bam \
   > $PWDS/${subjectID}.realigned.srt.stats



echo "Picard convertion ... completed! at:"
date




