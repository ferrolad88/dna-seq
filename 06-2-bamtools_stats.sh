#!/bin/bash
#$ -cwd

GLOBAL="${run_script}/global_config.sh"

if [[ -e $GLOBAL ]]
then
        . $GLOBAL
else
echo "Global config file not found. Exiting."
        exit 1
fi

# echo "bamtools stat ....  starting at:"
# date
# 
# echo "stats $PWDS/${subjectID}.mq.fxmt.bam " > $PWDS/${subjectID}.mq.fxmt.stats

# ${bamtools} stats \
#    -insert \
#    -in $PWDS/${subjectID}.mq.fxmt.bam \
#    >> $PWDS/${subjectID}.mq.fxmt.stats
# 
# echo "bamtools stat .... done at:"
# date

mv $PWDS/${subjectID}.mq.srt.stats  $PWDS/${subjectID}.mq.fxmt.stats

