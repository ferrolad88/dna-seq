#!/bin/bash
#$ -cwd

GLOBAL="${run_script}/global_config.sh"

if [[ -e $GLOBAL ]]
then
        . $GLOBAL
else
echo "Global config file not found. Exiting."
        exit 1
fi



echo "Picard validating ... starting at:"
date


# java -Xmx${heap}m -jar ${picard}/ValidateSamFile.jar \
#  I\=$PWDS/${subjectID}.mq.fxmt.bam \
#  O\=$PWDS/${subjectID}.mq.fxmt.bam.picard_vlid_rpt.txt


echo "Picard validation  ... done at:"
date




