#!/bin/bash
#$ -cwd


GLOBAL="${run_script}/global_config.sh"

if [[ -e $GLOBAL ]]
then
        . $GLOBAL
else
echo "Global config file not found. Exiting."
        exit 1
fi


echo "Picard HsMetrics ... starting at:"
date

# CalculateHsMetrics
# Calculates a set of Hybrid Selection specific metrics from an aligned SAMor BAM file. If a reference sequence is provided, AT/GC dropout metrics will be calculated, and the PER_TARGET_COVERAGE option can be used to output GC and mean coverage information for every target.
# Option	Description
# BAIT_INTERVALS=File	An interval list file that contains the locations of the baits used. Required.
# TARGET_INTERVALS=File	An interval list file that contains the locations of the targets. Required.
# INPUT=File	An aligned SAM or BAM file. Required.
# OUTPUT=File	The output file to write the metrics to. Required. Cannot be used in conjuction with option(s) METRICS_FILE (M)
# METRICS_FILE=File	Legacy synonym for OUTPUT, should not be used. Required. Cannot be used in conjuction with option(s) OUTPUT (O)
# METRIC_ACCUMULATION_LEVEL=MetricAccumulationLevel	The level(s) at which to accumulate metrics. Possible values: {ALL_READS, SAMPLE, LIBRARY, READ_GROUP} This option may be specified 0 or more times. This option can be set to 'null' to clear the default list.
# REFERENCE_SEQUENCE=File	The reference sequence aligned to. Default value: null.
# PER_TARGET_COVERAGE=File	An optional file to output per target coverage information to. Default value: null.

java -Xmx${heap}m -jar ${picard}/CalculateHsMetrics.jar \
 BAIT_INTERVALS\=$BaitFilePicard  \
 TARGET_INTERVALS\=$ExonFilePicard   \
 INPUT\=$PWDS/${subjectID}.realigned.recal.bam  \
 OUTPUT\=$PWDS/${subjectID}.realigned.recal.hsmetrics	

# PER_TARGET_COVERAGE\=$PWDS/${subjectID}.realigned.recal.hsmetrics.target.coverage



echo "Picard validation  ... done at:"
date




