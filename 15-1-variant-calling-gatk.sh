#!/bin/sh
#$ -S /bin/sh
#$ -cwd
#$ -l h_vmem=5G,time=8::

GLOBAL="${run_script}/global_config.sh"

if [[ -e $GLOBAL ]]
then
        . $GLOBAL
else
echo "Global config file not found. Exiting."
        exit 1
fi

# SEPARATE

subjectDir=$1
subjectID=$2

REF=$hg19_ref
PWDS="$subjectDir/analysis"
vargtkdir="$PWDS/variants-gatk"

mydate=`date` && echo -e "${subjectID} qsub GATK job submitted : $mydate"

echo "============== 1- GATK UnifiedGenotyper snps.raw.vcf  ================"
echo " "


mkdir $vargtkdir

rm -f $vargtkdir/*.idx

java -Xmx${heap}m  \
    -jar $gatk \
    -R $REF \
    -T UnifiedGenotyper \
    --dbsnp $DBSNP \
    -I $PWDS/${subjectID}.realigned.recal.bam \
    --out $vargtkdir/${subjectID}.snps.raw.vcf \
    -stand_call_conf 30.0 \
    -stand_emit_conf 10.0 \
    -out_mode EMIT_VARIANTS_ONLY \
    -l INFO \
    -A DepthOfCoverage \
    -A HaplotypeScore \
    -A InbreedingCoeff \
    -glm SNP  \
    -nt 1  \
    -L $ExonFile


rm -f $vargtkdir/*.idx

echo "============== 2- GATK UnifiedGenotyper indels.raw.vcf  ================"
echo " "


java -Xmx${heap}m  \
    -jar $gatk \
    -R $REF \
    -T UnifiedGenotyper \
    --dbsnp $DBSNP \
    -I $PWDS/${subjectID}.realigned.recal.bam \
    --out $vargtkdir/${subjectID}.indels.raw.vcf \
    -stand_call_conf 30.0 \
    -stand_emit_conf 10.0 \
    -out_mode EMIT_VARIANTS_ONLY \
    -l INFO \
    -A DepthOfCoverage \
    -A HaplotypeScore \
    -A InbreedingCoeff \
    -glm INDEL  \
    -nt 1  \
    -L $ExonFile


echo "============== 3- GATK VariantFiltration snps.vcf  ================"
echo " "


java -Xmx${heap}m  \
  -jar $gatk \
  -l INFO -T VariantFiltration \
  -R $REF \
  -o $vargtkdir/${subjectID}.indels.vcf \
  -V:VCF $vargtkdir/${subjectID}.indels.raw.vcf \
  --filterExpression "QD < 2.0" \
  --filterName "QDFilter" \
  --filterExpression "ReadPosRankSum < -20.0" \
  --filterName "ReadPosFilter" \
  --filterExpression "FS > 200.0" \
  --filterName "FSFilter" \
  --filterExpression "MQ0 >= 4 && ((MQ0 / (1.0 * DP)) > 0.1)" \
  --filterName "HARD_TO_VALIDATE" \
  --filterExpression "QUAL < 30.0 || DP < 6 || DP > 5000 || HRun > 5" \
  --filterName "QualFilter"


mline2=`grep -n "#CHROM" $vargtkdir/${subjectID}.indels.vcf | cut -d':' -f 1`
head -n $mline2 $vargtkdir/${subjectID}.indels.vcf > $vargtkdir/headindels.vcf
cat $vargtkdir/${subjectID}.indels.vcf | grep PASS | cat $vargtkdir/headindels.vcf - > $vargtkdir/${subjectID}.indels.PASS.vcf


echo "============== 4- GATK VariantFiltration indels.vcf  ================"
echo " "



java -Xmx${heap}m  \
  -jar $gatk \
  -l OFF -T VariantFiltration \
  -R $REF \
  -o $vargtkdir/${subjectID}.snps.vcf \
  --variant $vargtkdir/${subjectID}.snps.raw.vcf \
  --mask $vargtkdir/${subjectID}.indels.PASS.vcf \
  --maskName InDel \
  --clusterSize 3 \
  --clusterWindowSize 10 \
  --filterExpression "QD < 2.0" \
  --filterName "QDFilter" \
  --filterExpression "MQ < 40.0" \
  --filterName "MQFilter" \
  --filterExpression "FS > 60.0" \
  --filterName "FSFilter" \
  --filterExpression "HaplotypeScore > 13.0" \
  --filterName "HaplotypeScoreFilter" \
  --filterExpression "MQRankSum < -12.5" \
  --filterName "MQRankSumFilter" \
  --filterExpression "ReadPosRankSum < -8.0" \
  --filterName "ReadPosRankSumFilter" \
  --filterExpression "QUAL < 30.0 || DP < 6 || DP > 5000 || HRun > 5" \
  --filterName "StandardFilters" \
  --filterExpression "MQ0 >= 4 && ((MQ0 / (1.0 * DP)) > 0.1)" \
  --filterName "HARD_TO_VALIDATE"




echo "============== 5- GATK snps & indels PASS.vcf  ================"
echo " "


mline=`grep -n "#CHROM" $vargtkdir/${subjectID}.snps.vcf | cut -d':' -f 1`
head -n $mline $vargtkdir/${subjectID}.snps.vcf > $vargtkdir/head.vcf
cat $vargtkdir/${subjectID}.snps.vcf | grep PASS | cat $vargtkdir/head.vcf - > $vargtkdir/${subjectID}.snps.PASS.vcf

rm -f $vargtkdir/head.vcf
rm -f $vargtkdir/headindels.vcf


echo "============== 6- GATK snps eval  ================"
echo " "

java -Xmx${heap}m  \
   -jar $gatk \
   -T VariantEval \
   --dbsnp $DBSNP \
   -R $REF \
   -eval $vargtkdir/${subjectID}.snps.PASS.vcf \
   -o $vargtkdir/${subjectID}.snps.PASS.vcf.eval



echo "============== 7- GATK call all vcf  ================"
echo " "

rm -f $subjectDir/*.idx

java -Xmx${heap}m  \
    -jar $gatk \
    -R $REF \
    -T UnifiedGenotyper \
    --dbsnp $DBSNP \
    -I $PWDS/${subjectID}.realigned.recal.bam \
    --out $PWDS/${subjectID}.snps.raw.all.vcf \
    -stand_call_conf 30.0 \
    -stand_emit_conf 10.0 \
    -l INFO \
    -A DepthOfCoverage \
    -A HaplotypeScore \
    -A InbreedingCoeff \
    -glm SNP  \
    -nt 1  \
    -L $ExonFile \
   -U ALLOW_UNINDEXED_BAM \
   --output_mode EMIT_ALL_CONFIDENT_SITES



java -Xmx${heap}m  \
  -jar $gatk \
  -T VariantFiltration \
  -R $REF \
  -o $PWDS/${subjectID}.snps.all.vcf \
  --variant $PWDS/${subjectID}.snps.raw.all.vcf \
  --mask $vargtkdir/${subjectID}.indels.PASS.vcf \
  --maskName InDel \
  --filterExpression "QD < 2.0" \
  --filterName "QDFilter" \
  --filterExpression "MQ < 40.0" \
  --filterName "MQFilter" \
  --filterExpression "FS > 60.0" \
  --filterName "FSFilter" \
  --filterExpression "HaplotypeScore > 13.0" \
  --filterName "HaplotypeScoreFilter" \
  --filterExpression "MQRankSum < -12.5" \
  --filterName "MQRankSumFilter" \
  --filterExpression "ReadPosRankSum < -8.0" \
  --filterName "ReadPosRankSumFilter" \
  --filterExpression "QUAL < 30.0 || DP < 6 || DP > 5000 || HRun > 5" \
  --filterName "StandardFilters" \
  --filterExpression "MQ0 >= 4 && ((MQ0 / (1.0 * DP)) > 0.1)" \
  --filterName "HARD_TO_VALIDATE" \
  -l OFF


#  --quiet_output_mode

mline=`grep -n "#CHROM" $PWDS/${subjectID}.snps.all.vcf | cut -d':' -f 1`
head -n $mline $PWDS/${subjectID}.snps.all.vcf > $PWDS/head.vcf
cat $PWDS/${subjectID}.snps.all.vcf | grep PASS | cat $PWDS/head.vcf - > $PWDS/${subjectID}.snps.all.PASS.vcf



rm -f $PWDS/head.vcf
rm -f $PWDS/${subjectID}.snps.vcf
rm -f $PWDS/${subjectID}.snps.raw.all.vcf
rm -f $PWDS/*.idx
rm -f $subjectDir/*.idx
rm -f $vargtkdir/*.idx

mydate=`date` && echo -e "${subjectID} qsub GATK job completed : $mydate"
























