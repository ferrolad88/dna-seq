#!/bin/sh
#$ -S /bin/sh
#$ -cwd
#$ -l h_vmem=5G,time=8::

GLOBAL="${run_script}/global_config.sh"

if [[ -e $GLOBAL ]]
then
        . $GLOBAL
else
echo "Global config file not found. Exiting."
        exit 1
fi

# 
# date
# 
# echo "Fixing the mate pairs of realigned reads"
# 
# mkdir -p ${tmp_folder}_fixmate 
# 
# 
# java -Xmx${heap}m -Djava.io.tmpdir\=${tmp_folder}_fixmate \
#   -jar ${picard}/FixMateInformation.jar \
#   INPUT\=$PWDS/${subjectID}.mq.srt.bam \
#   OUTPUT\=$PWDS/${subjectID}.mq.fxmt.bam \
#   SO\=coordinate \
#   CREATE_INDEX\=true  \
#   VALIDATION_STRINGENCY\=SILENT
# 
# rm -rf ${tmp_folder}_fixmate
# 
# 
# echo "Fixing mate pairs completed!"
# date

mv $PWDS/${subjectID}.mq.srt.bam        $PWDS/${subjectID}.mq.fxmt.bam
mv $PWDS/${subjectID}.mq.srt.bai  -f    $PWDS/${subjectID}.mq.fxmt.bai
mv $PWDS/${subjectID}.mq.srt.bam.bai -f $PWDS/${subjectID}.mq.fxmt.bai



