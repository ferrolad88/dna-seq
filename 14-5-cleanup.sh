#!/bin/sh
#$ -S /bin/sh
#$ -cwd
#$ -l h_vmem=5G,time=8::

GLOBAL="${run_script}/global_config.sh"

if [[ -e $GLOBAL ]]
then
        . $GLOBAL
else
echo "Global config file not found. Exiting."
        exit 1
fi

echo "============== 14-5 Cleanup.sh  ================"
echo " "


# /md2/01-script/illuminizer_v8/cleanup.sh    SID28750
# subjectID=$1
# PWDS="/md2/Synaptome_Year2/part2-analysis/$subjectID/analysis"

rm -rf ${PWDS}/alignments
#analyzeCovar_v1
#analyzeCovar_v2
rm -rf ${PWDS}/${subjectID}
#rm -rf ${PWDS}/${subjectID}.bam
#rm -rf ${PWDS}/${subjectID}.bam.bai
rm -rf ${PWDS}/${subjectID}.srt.bam
rm -rf ${PWDS}/${subjectID}.srt.bam.bai
#rm -rf ${PWDS}/${subjectID}.srt.stats
rm -rf ${PWDS}/${subjectID}.fxmt.bai
rm -rf ${PWDS}/${subjectID}.fxmt.bam
rm -rf ${PWDS}/${subjectID}.fxmt.flt.bam
rm -rf ${PWDS}/${subjectID}.fxmt.flt.bam.bai
# rm -rf ${PWDS}/${subjectID}.fxmt.flt.stats
# rm -rf ${PWDS}/${subjectID}.fxmt.stats
# rm -rf ${PWDS}/${subjectID}.duplicate_report.txt
rm -rf ${PWDS}/${subjectID}.rmdup.bam
#rm -rf ${PWDS}/${subjectID}.rmdup.stats
rm -rf ${PWDS}/${subjectID}.mq.bam
rm -rf ${PWDS}/${subjectID}.mq.fxmt.bai
rm -rf ${PWDS}/${subjectID}.mq.fxmt.bam
#rm -rf ${PWDS}/${subjectID}.mq.fxmt.bam.picard_vlid_rpt.txt
#rm -rf ${PWDS}/${subjectID}.mq.fxmt.stats
rm -rf ${PWDS}/${subjectID}.mq_raw.bam
rm -rf ${PWDS}/${subjectID}.mq_raw_f1.uniq
rm -rf ${PWDS}/${subjectID}.mq.srt.bai
rm -rf ${PWDS}/${subjectID}.mq.srt.bam
#rm -rf ${PWDS}/${subjectID}.mq.srt.stats
rm -rf ${PWDS}/${subjectID}.flt.recal_v1.csv
rm -rf ${PWDS}/${subjectID}.flt.recal_v2.csv
rm -rf ${PWDS}/${subjectID}.forRealign.intervals
rm -rf ${PWDS}/${subjectID}.realigned.bam
rm -rf ${PWDS}/${subjectID}.realigned.fxmt.bai
rm -rf ${PWDS}/${subjectID}.realigned.fxmt.bam
rm -rf ${PWDS}/${subjectID}.realigned.srt.bai
rm -rf ${PWDS}/${subjectID}.realigned.srt.bam
#rm -rf ${PWDS}/${subjectID}.realigned.srt.stats
#rm -rf ${PWDS}/${subjectID}.realigned.recal.bam
#rm -rf ${PWDS}/${subjectID}.realigned.recal.bam.bai
#coverage
#gatk-variants
echo " "
mydate=`date` && echo -e "============== 14-5 Cleanup completed ... Variant calling starting at  $mydate ================"
echo " "

