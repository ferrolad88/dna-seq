#!/bin/bash
#$ -cwd

GLOBAL="${run_script}/global_config.sh"

if [[ -e $GLOBAL ]]
then
        . $GLOBAL
else
echo "Global config file not found. Exiting."
        exit 1
fi

echo "============== 05-3 picard remove duplicates ================"
echo " "
echo "Marking duplicates in ${subjectID}.bam ... started at:"
date

#   AS\=true  \

mkdir -p ${tmp_folder}_rmdup 

java -Xmx${heap}m -Djava.io.tmpdir\=${tmp_folder}_rmdup \
   -jar ${picard}/MarkDuplicates.jar \
   I\=$PWDS/${subjectID}.fxmt.flt.bam \
   O\=$PWDS/${subjectID}.rmdup.bam \
   M\=$PWDS/${subjectID}.duplicate_report.txt \
   VALIDATION_STRINGENCY\=SILENT \
   REMOVE_DUPLICATES\=true


java -Xmx${heap}m -Djava.io.tmpdir\=${tmp_folder}_rmdup  \
  -jar ${picard}/AddOrReplaceReadGroups.jar \
   RGLB\=${subjectID}.fastq \
   RGPL\=Illumina \
   RGPU\=MPIR1 \
   RGSM\=MP1 \
   I\=$PWDS/${subjectID}.rmdup.bam \
   O\=$PWDS/${subjectID}.rmdup.g.bam \
   SORT_ORDER\=coordinate \
   CREATE_INDEX\=true \
   VALIDATION_STRINGENCY\=SILENT

mv -f $PWDS/${subjectID}.rmdup.g.bam $PWDS/${subjectID}.rmdup.bam
# mv -f $PWDS/${subjectID}.rmdup.g.bam.bai $PWDS/${subjectID}.rmdup.bai
mv -f $PWDS/${subjectID}.rmdup.g.bai     $PWDS/${subjectID}.rmdup.bai


${bamtools} stats \
   -insert \
   -in $PWDS/${subjectID}.rmdup.bam \
   > $PWDS/${subjectID}.rmdup.stats


rm -rf ${tmp_folder}_rmdup

echo "Marking duplicates output: ${subjectID}.rmdup.bam  .... done at:"
date





