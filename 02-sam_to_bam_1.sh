#!/bin/bash
#$ -cwd

GLOBAL="${run_script}/global_config.sh"

if [[ -e $GLOBAL ]]
then
        . $GLOBAL
else
echo "Global config file not found. Exiting."
        exit 1
fi

echo "============== 02-sam to bam ================"
echo " "

echo "Sorting and convert to bam ... starting at:"
date

# convert to bam
${samtools} view -uS $PWDS/${subjectID}.$lane_num1.aligned.sam.gz  \
  |  ${samtools} sort -m 3000000000 - $PWDS/${subjectID}.$lane_num1.aligned.srt

echo "Sorting and convert to bam ... done at:"
date

